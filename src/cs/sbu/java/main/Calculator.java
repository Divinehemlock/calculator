package cs.sbu.java.main;

import java.util.*;


public class Calculator {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        float a, b;
        float answer = 0;
        char operator;
        System.out.print("Please enter first number: ");
        a = input.nextFloat();
        System.out.print("Please enter operator: ");
        operator = input.next().charAt(0);
        System.out.print("Please enter second number: ");
        b = input.nextFloat();
        switch (operator) {
            case '+':
                answer = sumFunction(a, b);
                break;
            case '-':
                answer = subFunction(a, b);
                break;
            case '/':
                answer = divideFunction(a, b);
                break;
            case '*':
                answer = productFunction(a, b);
                break;
        }
        System.out.print(Float.toString(a) + operator + b + '=' + answer);
    }

    //Sum Function:
    private static float sumFunction(float a, float b) {
        return a + b;
    }

    //Subtract Function:
    private static float subFunction(float a, float b) {
        return a - b;
    }

    //Divide Function:
    private static float divideFunction(float a, float b) {
        return a / b;
    }

    //Product Function:
    private static float productFunction(float a, float b) {
        return a * b;
    }
}
